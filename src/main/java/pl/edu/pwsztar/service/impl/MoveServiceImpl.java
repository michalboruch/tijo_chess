package pl.edu.pwsztar.service.impl;

import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.MoveService;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class MoveServiceImpl implements MoveService {

    private Integer convertCharToInt(String value){
        Map<String, Integer> position = new LinkedHashMap<>();
        position.put("a", 1);
        position.put("b", 2);
        position.put("c", 3);
        position.put("d", 4);
        position.put("e", 5);
        position.put("f", 6);
        position.put("g", 7);
        position.put("h", 8);

        return position.get(value);
    }

    @Override
    public Boolean moveBishop(FigureMoveDto figureMoveDto) {
        String[] startPosition = figureMoveDto.getStart().split("_");
        String[] destinationPosition = figureMoveDto.getDestination().split("_");

        /* rows - a, b, c...
         * columns - 1, 2, 3... */

        int currentRow = convertCharToInt(startPosition[0]);
        int currentColumn = Integer.parseInt(startPosition[1]);
        int destinationRow = convertCharToInt(destinationPosition[0]);
        int destinationColumn = Integer.parseInt(destinationPosition[1]);

        return Math.abs(currentColumn - destinationColumn) == Math.abs(currentRow - destinationRow);
    }
}
