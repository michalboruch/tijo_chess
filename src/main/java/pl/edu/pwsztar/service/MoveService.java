package pl.edu.pwsztar.service;

import pl.edu.pwsztar.domain.dto.FigureMoveDto;

public interface MoveService {
    Boolean moveBishop(FigureMoveDto figureMoveDto);
}
